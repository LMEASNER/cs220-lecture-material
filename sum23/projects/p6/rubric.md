# Project 6 (P6) grading rubric

## Code reviews

- A TA / grader will be reviewing your code after the deadline.
- They will make deductions based on the Rubric provided below.
- To ensure that you don’t lose any points in code review, you must review the rubric and make sure that you have followed the instructions provided in the project correctly.

## Rubric

### General deductions:

- Did not save the notebook file prior to running the cell containing "export". We cannot see your output if you do not save before generating the zip file. This deduction will become stricter for future projects. (-3)
- Used concepts/modules (ex: dictionaries, or pandas) not covered in class yet (It is okay to use built-in functions that you have been instructed to use) (-3)
- import statements are not mentioned in the required cell at the top of the notebook (-1)

### Question specific deductions:

- cell (1)
	- Function is defined more than once (-1)

- Q1 (4)
	- Incorrect logic is used to answer (-2)
	- Required functions are not used to answer (-1)

- Q2 (3)
	- Incorrect logic is used to answer (-2)
	- Required functions are not used to answer (-1)

- Q3 (3)
	- Incorrect logic is used to answer (-2)
	- Required functions are not used to answer (-1)

- find_room_names (4)
	- Function logic is incorrect (-2)
	- Function is defined more than once (-1)
	- Required functions are not used to answer (-1)

- Q4 (3)
	- Required functions are not used to answer (-2)

- Q5 (4)
	- Required functions are not used to answer (-2)
	- Incorrect logic is used to answer (-1)

- Q6 (4)
	- Incorrect logic is used to answer (-2)
	- Variable to store the name of the longest room is not initialized as `None` (-1)
	- Required functions are not used to answer (-1)

- Q7 (4)
	- Incorrect logic is used to answer (-2)
	- Required functions are not used to answer (-1)

- Q8 (4)
	- Incorrect logic is used to answer (-2)
	- Required functions are not used to answer (-1)

- Q9 (3)
	- Incorrect logic is used to answer (-2)
	- Required functions are not used to answer (-1)

- avg_price_per_room_type (5)
	- Function logic is incorrect (-2)
	- Function does not return `None` if there are no rooms of the type room_type in the given neighborhood (-1)
	- Function is defined more than once (-1)
	- Required functions are not used to answer (-1)

- Q10 (3)
	- Required functions are not used to answer (-2)

- Q11 (3)
	- Required functions are not used to answer (-2)

- find_prices_within (4)
	- Function logic is incorrect (-2)
	- Function is defined more than once (-1)
	- Required functions are not used to answer (-1)

- Q12 (3)
	- Required functions are not used to answer (-2)

- median (3)
	- Function logic is incorrect (-2)
	- Function is defined more than once (-1)

- Q13 (3)
	- Required functions are not used to answer (-2)

- Q14 (4)
	- Incorrect logic is used to answer (-2)
	- Required functions are not used to answer (-1)

- avg_review_avail_ratio (6)
	- Function logic is incorrect (-3)
	- Function does not return `None` if there are no rooms in the `neighborhood` for which the ratio can be computed (-1)
	- Function is defined more than once (-1)
	- Required functions are not used to answer (-1)

- Q15 (3)
	- Required functions are not used to answer (-2)

- Q16 (3)
	- Required functions are not used to answer (-2)

- Q17 (5)
	- Incorrect logic is used to answer (-2)
	- `max_nbhd_staten_island` is not initialized as `None` (-1)
	- Duplicates are not removed before looping through neighborhoods in Staten Island (-1)
	- Required functions are not used to answer (-1)

- find_good_rooms (6)
	- Function logic is incorrect (-2)
	- Function does not return `None` if the average price cannot be computed due to missing data (-1)
	- `avg_price_per_room_type` function is called more often than necessary (-1)
	- Required functions are not used to answer (-1)
	- Function is defined more than once (-1)

- Q18 (3)
	- Required functions are not used to answer (-2)

- Q19 (4)
	- Required functions are not used to answer (-2)
	- Incorrect logic is used to answer (-1)

- Q20 (5)
	- Incorrect logic is used to answer (-3)
	- Required functions are not used to answer (-1)
